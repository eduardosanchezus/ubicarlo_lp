<?php

/*!
* Developed by: Chris Q.
* Year: 2019
*
* Depends:
*   PHP 4
*/

// timestamp: 28th - 04 - 2019

$subject = "eBeards | ".json_decode('"\uD83D\uDE80"')." Thanks for contacting us!";


$message_client = "
<!DOCTYPE html>
<html>
<head>
	<title>Client message</title>
	<meta charset='utf-8'>
</head>
<body style='background-color: #f5f5f5;font-family: sans-serif;padding: 0px;margin: 0px;'>

<a href='https://ebeards.com' style='text-decoration: none;color: #0d56c9;font-weight: bold;'>
	<img src='https://ebeards.com/email_templates/logo.png' alt='www.eBeards.com' style='display: block;text-align: center;font-size: 40px;text-decoration: none;margin: 0 auto;padding: 30px 0px;'>
</a>

<div style='display: block;margin: 20px auto 0px;width: 640px;background-color: white;min-height: 200px;border-radius: 8px;box-shadow: 0px 14px 31px 0px rgba(0,0,0,0.1);box-sizing: border-box;padding: 20px 20px;border: 1px solid #d9d9d9;'>

	<h1 style='display: block;text-align: center;text-transform: uppercase;font-size: 28px;color: #2d2d2d;'>
		Thanks!
	</h1>

	<p style='text-align: center;'>
		Your information have been sent<br>
		This information is completely private, your privacy is very important for us!
	</p>

	<br>

	<p style='text-align: center;margin: 30px 0px;'>
		<span style='width: 30%;text-align: right;display: inline-block;padding: 0px 5px;'>
			Name:
		</span>
		<span style='width: 50%;text-align: left;display: inline-block;padding: 0px 5px;'>
			$f_name
		</span>
	</p>

	<p style='text-align: center;margin: 30px 0px;'>
		<span style='width: 30%;text-align: right;display: inline-block;padding: 0px 5px;'>
			E-Mail:
		</span>
		<span style='width: 50%;text-align: left;display: inline-block;padding: 0px 5px;'>
			$e_mail
		</span>
	</p>

	<p style='text-align: center;margin: 30px 0px;'>
		<span style='width: 30%;text-align: right;display: inline-block;padding: 0px 5px;'>
			Phone:
		</span>
		<span style='width: 50%;text-align: left;display: inline-block;padding: 0px 5px;'>
			$u_phone
		</span>
	</p>

	<p style='text-align: center;margin: 30px 0px;'>
		<span style='width: 30%;text-align: right;display: inline-block;padding: 0px 5px;'>
			Interest:
		</span>
		<span style='width: 50%;text-align: left;display: inline-block;padding: 0px 5px;'>
			$u_interest
		</span>
	</p>

	<p style='text-align: center;margin: 30px 0px;'>
		<span style='width: 30%;text-align: right;display: inline-block;vertical-align: top;padding: 0px 5px;'>
			Message:
		</span>
		<span style='width: 50%;text-align: left;display: inline-block;vertical-align: top;padding: 0px 5px;'>
			$u_message
		</span>
	</p>
	
</div>


<div style='display: block;margin: 50px 0px 0px 0px;border-bottom: 10px solid #ef4a66;font-size: 16px;text-align: center; color: #666666;line-height: 1.6;padding-bottom: 20px;'>
	<p>
		This email was sent to <a style='color: #ef4a66;text-decoration: none;' href='mailto:$e_mail'>$e_mail</a>
		<br>
		San Jose, Costa Rica
		<br>
		© 2012-2019 eBeards All Rights Reserved.
	</p>
</div>

</body>
</html>
";


// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
$mail->CharSet = 'utf-8';

try {
    //Server settings
    $mail->SMTPDebug = 2;                                       // Enable verbose debug output
    $mail->isSMTP();                                            // Set mailer to use SMTP
    $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'info@ebeards.com';                     // SMTP username
    $mail->Password   = 'Hello123!';                            // SMTP password
    $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('info@ebeards.com', 'eBeards');
    //$mail->addAddress($e_mail, $f_name);     // Add a recipient
    $mail->addAddress($e_mail);                                  // Name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    // Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $message_client;
    $mail->AltBody = 'Message sent by eBeards - From: info@ebeards.com | Thanks for contacting us!';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>