/*!
* Developed by: v28 , Chris Q.
* Year: 2018
*
* Depends:
*   jQuery
*/

// timestamp: 16th - 07 - 2018

function consoleSecurityAlert()
{
	console.log('%cStop!', 'background: transparent; color: red; font-size: 40px;');console.log('%cThis is a browser feature intended for developers. If someone told you to copy-paste something here to enable a feature or "hack" someone\'s account, it is a scam and will give them access to your accounts.', 'background: transparent; color: green; font-size: 16px;');
}

function ion_tabs()
{
	if ($('section.box-2 div.buttons button').length)
	{
		butonsTabs = $('section.box-2 div.buttons button');

		butonsTabs.click(function(event)
		{
			butonsTabs.removeClass('active');
			$(this).addClass('active');
			thisAttr   = $(this).attr('data-activethis');

			$('section.box-2 div.wrapper').removeClass('active')
			$('section.box-2 div.wrapper.'+thisAttr).addClass('active')
		});
	}
}
ion_tabs();


$('header div.wrapper > div.inl nav ul li button').click(function(event)
{
	$('div#float-menu').addClass('active');
	$('div#float-menu-close').show();
});

$('div#float-menu button.cancel , div#float-menu ul li a').click(function(event)
{
	$('div#float-menu').removeClass('active');
});

$('div#float-menu-close').click(function(event)
{
	$('div#float-menu').removeClass('active');
	$(this).hide();
});

$(window).scroll(function(event)
{
	if ($(window).scrollTop() > 400)
	{
		$('header.fixed').addClass('onscreen');
	} else {
		$('header.fixed').removeClass('onscreen');
	}
});