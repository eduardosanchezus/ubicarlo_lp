<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="theme-color" content="#ffffff">
	<!--Start SEO Tags-->
	<meta name="description" content="Template Description">
	<!--Start Social Tags-->
	<meta property="og:title" content="Template">
	<meta property="og:image" content="img/social-seo.jpg">
	<meta property="og:description" content="Template Description">
	<meta charset="utf-8">
	<!-- Twitter Card data -->
	<meta name="twitter:card" value="Template Description">
	<!-- Twitter Summary card images must be at least 120x120px -->
	<meta name="twitter:image" content="img/social-seo.jpg">
	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Template">
	<meta itemprop="description" content="Template Description">
	<meta itemprop="image" content="img/social-seo.jpg">
	<!--Start HTML5 Tags-->
	<title>Ubicarlo</title>
	<link rel="icon" type="image/png" href="img/favicon.png">
	<link rel="image_src" href="img/social-seo.jpg">
	<!--Start CSS Codes-->
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
<!--[if lt IE 10]>
<div class="alert-old">
	You are using an <strong>outdated</strong> browser. Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<section id="coming-soon">
	<div class="wrapper">

		<header>
			<a href="/" class="logo">
				<img class="inl" src="img/logo1.png">
			</a>

			<nav>
				<ul>
					<li>
						<a href="mailto: eduardo@ionchargers.com">
							<i class="far fa-envelope"></i>
						</a>
					</li>
					<li>
						<a href="tel:+50688373662">
							<i class="fas fa-phone"></i>
						</a>
					</li>
					<li>
						<a href="https://facebook.com/ionchargers">
							<i class="fab fa-facebook-square"></i>
						</a>
					</li>
					<li>
						<a href="https://api.whatsapp.com/send?phone=50688373662">
							<i class="fab fa-whatsapp"></i>
						</a>
					</li>
				</ul>
			</nav>
		</header>

		<div class="left">
			
			<h1>
				Ubicarlo
			</h1>
			<p class="caption1">
				Muy pronto!
			</p>
			<p>
				Estaremos listos muy pronto, si tienes preguntas por favor envíanos un correo o llena el formulario.
			</p>

		</div><!--Left-->

		<div class="right">
			<form id="contact_form_02" method="POST">
				<input required type="text" name="fname" placeholder="Nombre">
				<input type="hidden" name="lname">
				<input required type="text" name="phone" placeholder="Teléfono">
				<input required type="text" name="email" placeholder="Correo Electrónico">
			<br>
			<button class="inl cta">
				ENVIAR <i class="fas fa-map-marker-alt"></i>
			</button>
			</form>
		</div><!--Right-->

		<div class="bottom">
			<p>
				2019 Todos los derechos reservados | Ubicarlo
			</p>
		</div>
	</div>
</section>

<!--
** jQuery
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.12.4.min.js">\x3C/script>');</script>
<!--
** prefixfree
-->
<script type="text/javascript">
function loadLocalprefixFree()
{
	document.write('<script src="js/prefixfree.min.js">\x3C/script>');
}
</script>
<script onerror="loadLocalprefixFree()" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
<!--[if lt IE 9]>
<script src="js/old-support/IE9.js"></script>
<![endif]-->
<!--
** Local
-->
<script type="text/javascript" src="js/scripts.main.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

</body>
</html>