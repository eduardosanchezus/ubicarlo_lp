<?php

	include_once 'config.php';

	$url = $apiURL . $list_id . '/members/' . $_COOKIE['eb-mail'] . '/tags/';
	
	$pfb_data = array(
	    'tags'  => array( 
	    	array(
		      'name' => 'cart',
		      'status' => 'active' // active and inactive
		    )
	    )
	  );

	$encoded_pfb_data = json_encode($pfb_data);

	// Setup cURL sequence
	$ch = curl_init();

	/* ================
	* cURL OPTIONS
	* The tricky one here is the _USERPWD - this is how you transfer the API key over
	* _RETURNTRANSFER allows us to get the response into a variable which is nice
	* This example just POSTs, we don't edit/modify - just a simple add to a list
	* _POSTFIELDS does the heavy lifting
	* _SSL_VERIFYPEER should probably be set but I didn't do it here
	* ================
	*/
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded_pfb_data);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$results = curl_exec($ch); // store response
	$response = curl_getinfo($ch, CURLINFO_HTTP_CODE); // get HTTP CODE
	$errors = curl_error($ch); // store errors

	curl_close($ch);

	// Returns info back to jQuery .ajax or just outputs onto the page

	$results = array(
	'response' => $response,
	'errors' => $errors
	);


	// Sends data back to the page OR the ajax() in your JS
	echo json_encode($results);


?>