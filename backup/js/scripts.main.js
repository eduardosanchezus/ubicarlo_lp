/*!
* Developed by: v28 , Chris Q.
* Year: 2018
*
* Depends:
*   jQuery
*/

// timestamp: 16th - 07 - 2018

$('#contact_form_02').submit(function(event)
{
	formID = $('#contact_form_02');

	$.post('../api/mailchimp/step1.php', {
		fname: $('[name=fname]').val(),
		lname: $('[name=lname]').val(),
		u_phone: $('[name=phone]').val(),
		u_email: $('[name=email]').val(),
	}, function(data, textStatus, xhr)
	{
		window.location = 'https://ionchargers.com/thanks.php?u_name='+$('[name=fname]').val();
	});

	$('body').append('<div style="display: block;background-image: url(\'img/SVG-Loaders-master/svg-loaders/puff.svg\');position: fixed;width: 100%;height: 100%;background-position: center;background-repeat: no-repeat;z-index: 999;background-color: #444;background-size: 194px;top: 0;left: 0;"></div>');

	return false;
});