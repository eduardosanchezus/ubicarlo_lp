# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2017-06-20
### Added
- Item added

### Changed
- Item changed

### Removed
- Item removed

## [0.3.0] - 2015-12-03
### Added
- Item added here

## [0.2.0] - 2015-10-06
### Changed
- Changed this time

## [0.1.0] - 2015-10-06
### Added
- Answer "Question here".

### Changed
- Improve argument against commit logs.
- Start following [SemVer](https://semver.org) properly.

## [0.0.8] - 2015-02-17
### Changed
- Update year to match in every README example.
- Reluctantly stop making fun of Brits only, since most of the world
  writes dates in a strange way.

### Fixed
- Fix typos in recent README changes.
- Update outdated unreleased diff link.

## [0.0.7] - 2015-02-16
### Added
- Link, and make it obvious that date format is ISO 8601.

### Changed
- Clarified the section on "Is there a standard change log format?".

### Fixed
- Fixes goes here
