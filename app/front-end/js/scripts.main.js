/*!
* Developed by: v28 , Chris Q.
* Year: 2018
*
* Depends:
*   jQuery
*/

// timestamp: 16th - 07 - 2018

function consoleSecurityAlert()
{
	console.log('%cStop!', 'background: transparent; color: red; font-size: 40px;');console.log('%cThis is a browser feature intended for developers. If someone told you to copy-paste something here to enable a feature or "hack" someone\'s account, it is a scam and will give them access to your accounts.', 'background: transparent; color: green; font-size: 16px;');
}

$('#sidebar ul li button').click(function(event)
{
	href_data = $(this).attr('data-href');

	// Check the compact view
	if ($('body').hasClass('compact'))
	{
		$('body').removeClass('compact');
	}

	// Alerts screen
	function alerts_screen()
	{
		if ($('div#alerts-grid button').length)
		{
			$('div#alerts-grid button').click(function(event)
			{
				$('body').removeClass('compact');

				$('div#alerts-grid button').removeClass('active');

				setTimeout(function(){ $('body').addClass('compact'); }, 500);

				$(this).addClass('active');
			});

		}
	}

	// Historial grids
	function historial_calendar()
	{
		if ($("#data-choose").length)
		{
			$("input[name=data-choose]").flatpickr({
				enableTime: true,
				maxDate: "today",
				time_24hr: true,
				dateFormat: "Y-m-d",
			});

			// Activate the compact view
			$('body').addClass('compact');
		}
	}

	$('#sidebar ul li button').removeClass('active');
	$(this).addClass('active');

	//window.location = href_data;
	$('#app-screens').load(href_data,
		function()
		{
		// console.log('Loaded');
		alerts_screen();
		historial_calendar();
	});
	
});

// Preload activated screen
$(document).ready(function()
{
	$('#sidebar ul li button.active').trigger('click');
});

/*if ($('div#historial-box p').length)
{
	$('div#historial-box p').click(function(event)
	{
		mapHTML = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7859.622002989233!2d-84.15261316982674!3d9.949677599977635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2scr!4v1557536027001!5m2!1ses!2scr" frameborder="0" style="border:0" allowfullscreen></iframe>';

		$('div#historial-box p').removeClass('selected');
		$('div#historial-box p').find('iframe').remove();

		$(this).addClass('selected');

		$(this).append(mapHTML);
	});
}*/